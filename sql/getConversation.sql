USE spieler_test;
SELECT c.id, c.name, h.content, p.content
FROM conversation as c
INNER JOIN header as h
ON  c.id= h.id
INNER JOIN paragraph as p
ON  c.id = p.conversationId
INNER JOIN trailer as t
ON  c.id = t.id;