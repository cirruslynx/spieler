use spieler_test;

SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE header;
TRUNCATE TABLE trailer;
TRUNCATE TABLE conversation;
TRUNCATE TABLE paragraph;
SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO conversation
(`name`)
VALUES
("CONVERSATION 1");

SELECT  @ID := LAST_INSERT_ID();

INSERT INTO header
(`id`,`content`,`createdDate`)
VALUES
(@ID,'HEADER 1',curdate());

INSERT INTO trailer
(`id`, `content`,`createdDate`)
VALUES
(@ID,'TRAILER 1',curdate());

INSERT INTO paragraph
(`content`,`conversationId`,`createdDate`)
VALUES
("This is a paragraph in CONVERSATION 1. It has multiple sentences.",@ID,curdate());

INSERT INTO paragraph
(`content`,`conversationId`,`createdDate`)
VALUES
("This is another paragraph in CONVERSATION 1. It is part of the same conversation.",@ID,curdate());

INSERT INTO conversation
(`name`)
VALUES
("CONVERSATION 2");

SELECT  @ID := LAST_INSERT_ID();

INSERT INTO header
(`id`,`content`,`createdDate`)
VALUES
(@ID,'HEADER 2',curdate());

INSERT INTO trailer
(`id`, `content`,`createdDate`)
VALUES
(@ID,'TRAILER 2',curdate());

INSERT INTO paragraph
(`content`,`conversationId`,`createdDate`)
VALUES
("This is a paragraph in CONVERSATION 2",@ID,curdate());

INSERT INTO paragraph
(`content`,`conversationId`,`createdDate`)
VALUES
("This is another paragraph in CONVERSATION 2.",@ID,curdate());
