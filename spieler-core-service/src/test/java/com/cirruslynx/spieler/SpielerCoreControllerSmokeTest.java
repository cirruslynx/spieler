package com.cirruslynx.spieler;

import com.cirruslynx.spieler.controller.SpielerCoreController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class SpielerCoreControllerSmokeTest {

	@Autowired
	private SpielerCoreController controller;

	@Test
	void contextLoads() {
		assertThat(controller).isNotNull();
	}
}