package com.cirruslynx.spieler.service;

import com.cirruslynx.spieler.repository.HeaderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;


@SpringBootTest
class SpielerCoreServiceTest {

    @Value("${conversation.core.message}")
    private String message;

    @Value("${conversation.core.topic.name}")
    private String topicName;

    @InjectMocks
    private SpielerCoreService spielerCoreService;

    @Mock
    private SpielerKafkaProducer kafkaProducer;

    @Mock
    private HeaderRepository headerRepository;

    @Captor
    ArgumentCaptor<String>  topicNameCaptor;

    @BeforeEach
    public void setUp() {
        ReflectionTestUtils.setField(spielerCoreService, "message",message);
        ReflectionTestUtils.setField(spielerCoreService, "topicName", topicName);
    }

    //@Test
    void givenMessageIsSetwhenGetHeaderIsCalledThenReturnHeader() {

//        Header savedHeader = new Header(0L,message, LocalDateTime.now());
//        when(headerRepository.save(any(Header.class))).thenReturn(savedHeader);
//        HeaderDTO headerDTO =  spielerCoreService.getHeader(message);
//        verify(headerRepository).save(any(Header.class));
//        verify(kafkaProducer).send(topicNameCaptor.capture(),any(HeaderDTO.class));
//        assertEquals(topicName,topicNameCaptor.getValue());
//        assertEquals(headerDTO.getId(), savedHeader.getId());
//        assertEquals(headerDTO.getContent(), savedHeader.getContent());
//        assertEquals(headerDTO.getCreatedDate(), savedHeader.getCreatedDate());

    }
}
