package com.cirruslynx.spieler.controller;

import com.cirruslynx.spieler.domain.ConversationDTO;
import com.cirruslynx.spieler.domain.HeaderDTO;
import com.cirruslynx.spieler.domain.ParagraphDTO;
import com.cirruslynx.spieler.domain.TrailerDTO;
import com.cirruslynx.spieler.service.SpielerCoreService;
import com.cirruslynx.utilities.RandomIdGenerator;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest()
class SpielerCoreControllerTest
{
    final String TEST_NAME = "TEST-CONVERSATION";
    
    @InjectMocks
    private SpielerCoreController spielerCoreController;
     
    @Mock
    private SpielerCoreService spielerCoreService;

    @Mock
    private  RandomIdGenerator randomIdGenerator;

    @Test
    void given_UserIsNotAuthenticated_When_NameIsUsed_Then_ConversationIsCreated()
    {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        ConversationDTO conversationDTO = new ConversationDTO(1L,
                TEST_NAME,
                new HeaderDTO(),
                new ArrayList<ParagraphDTO>(),
                new TrailerDTO() );
        when(spielerCoreService.createConversation(any(String.class))).thenReturn(conversationDTO);
        when(randomIdGenerator.verifyOrCreateId(null)).thenReturn("ABCDE");
        when(response.getStatus()).thenReturn(200);
        ConversationDTO actualConversationDTO =  spielerCoreController.createConversation(TEST_NAME,request,response);

        verify(spielerCoreService).createConversation(TEST_NAME);
        verify(randomIdGenerator,times(1)).verifyOrCreateId(null);

        assertEquals(actualConversationDTO, conversationDTO);
        assertEquals(200,response.getStatus());
    }
     
}