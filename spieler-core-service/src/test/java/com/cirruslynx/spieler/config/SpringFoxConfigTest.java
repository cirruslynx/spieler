package com.cirruslynx.spieler.config;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;
import springfox.documentation.spring.web.plugins.Docket;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class SpringFoxConfigTest {

    private final ApplicationContextRunner contextRunner = new ApplicationContextRunner()
            .withUserConfiguration(SpringFoxConfig.class);

    @Test
    void whenConfigLoaded_BeanIsAvailable() {
        contextRunner.run(applicationContext -> {
            Boolean flag = applicationContext.containsBean("api");
            assertThat(applicationContext).hasSingleBean(Docket.class);
        });
    }
}