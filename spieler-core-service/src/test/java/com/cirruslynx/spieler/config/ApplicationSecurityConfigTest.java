package com.cirruslynx.spieler.config;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
class ApplicationSecurityConfigTest {

    private final ApplicationContextRunner contextRunner = new ApplicationContextRunner()
            .withUserConfiguration(ApplicationSecurityConfig.class);

    @Test
    void whenConfigLoaded_BeanIsAvailable() {
        contextRunner.run(applicationContext -> {
            Boolean flag = applicationContext.containsBean("accessToken");
           // assertThat(applicationContext).hasSingleBean(AccessToken.class);
        });
    }
}