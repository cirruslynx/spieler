package com.cirruslynx.spieler.config;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
class KeycloakSecurityConfigTest {

    private final ApplicationContextRunner context = new ApplicationContextRunner()
            .withUserConfiguration(KeycloakSecurityConfig.class);

    @Test
    void whenConfigLoaded() {
        context.run(it -> {
            Boolean flag = it.containsBean("keycloakConfigResolver");
            flag = it.containsBean("sessionAuthenticationStrategy");
           // assertThat(it).hasSingleBean(KeycloakConfigResolver.class);
        });
    }
}