package com.cirruslynx.spieler.controller;

import com.cirruslynx.spieler.domain.ConversationDTO;
import com.cirruslynx.spieler.service.SpielerCoreService;
import com.cirruslynx.utilities.RandomIdGenerator;
import lombok.extern.log4j.Log4j2;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.cirruslynx.utilities.CorrelationIdConstants.CORRELATION_ID_HEADER_NAME;
import static com.cirruslynx.utilities.CorrelationIdConstants.CORRELATION_ID_LOG_VAR_NAME;

@Log4j2
@RestController
@ComponentScan("com.cirruslynx.utilities")
public class SpielerCoreController {

	private final RandomIdGenerator randomIdGenerator;

	private final SpielerCoreService spielerCoreService;

	@Autowired
	public SpielerCoreController(RandomIdGenerator randomIdGenerator, SpielerCoreService spielerCoreService) {
		this.randomIdGenerator = randomIdGenerator;
		this.spielerCoreService = spielerCoreService;
	}

	@RolesAllowed("user")
	@PostMapping("/conversation")
	public ConversationDTO createConversation(@RequestParam(value = "name", defaultValue = "CONVERSATION") String name,
											  HttpServletRequest request, HttpServletResponse response) {
	    String correlationId = request.getHeader(CORRELATION_ID_HEADER_NAME);
		correlationId = randomIdGenerator.verifyOrCreateId(correlationId);
		log.trace("Using correlationId {}", correlationId);
		MDC.put(CORRELATION_ID_LOG_VAR_NAME, correlationId);
	    response.setHeader(CORRELATION_ID_HEADER_NAME, correlationId);
		log.debug("Conversation service is creating conversation {} ",  name);
		return spielerCoreService.createConversation(name);
	}
}
