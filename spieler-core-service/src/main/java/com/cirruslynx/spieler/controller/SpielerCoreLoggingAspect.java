package com.cirruslynx.spieler.controller;

import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;

@Log4j2
@Aspect
@Configuration
public class SpielerCoreLoggingAspect {

    @Before(value = "execution(* com.cirruslynx.spieler.controller.SpielerCoreController.*(..))")
    public void before(JoinPoint joinPoint) {
        log.trace("Entering SpielerCoreController");
    }

    @After(value = "execution(* com.cirruslynx.spieler.controller.SpielerCoreController.*(..))")
    public void afterReturning(JoinPoint joinPoint)
    {
        log.trace("Leaving SpielerCoreController");
    }

}