package com.cirruslynx.spieler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpielerCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpielerCoreApplication.class, args);
    }
}