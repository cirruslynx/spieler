package com.cirruslynx.spieler.service;

import com.cirruslynx.spieler.domain.ConversationDTO;
import com.cirruslynx.spieler.repository.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service
public class SpielerCoreService {

    @Value("${conversation.core.message}")
    private String message;

    @Value("${conversation.core.topic.name}")
    private String topicName;

    private final SpielerKafkaProducer kafkaProducer;

    private final ConversationRepository conversationRepository;

    @Autowired
    public SpielerCoreService(ConversationRepository  conversationRepository, SpielerKafkaProducer kafkaProducer) {
        this.conversationRepository = conversationRepository;
        this.kafkaProducer = kafkaProducer;
    }

    public ConversationDTO createConversation(final String name) {
        Conversation conversation = new Conversation(name);
        Header header = new Header(conversation, "HEADER1", LocalDateTime.now());
        Trailer trailer = new Trailer(conversation, "TRAILER1", LocalDateTime.now());
        Paragraph paragraph = new Paragraph(conversation, "THIS IS PARAGRAPH1", LocalDateTime.now());
        List<Paragraph> paragraphs = new ArrayList<Paragraph>();
        paragraphs.add(paragraph);
        conversation.setHeader(header);
        conversation.setParagraphs(paragraphs);
        conversation.setTrailer(trailer);
        conversationRepository.save(conversation);
        log.debug("Saving conversation {}  with id {} to data store", conversation.getName(), conversation.getId());
        ConversationDTO newConversation = SpielerRepositoryUtils.toConversationDTO(conversation);
        log.debug("Writing conversation {} to {}", newConversation, topicName);
        kafkaProducer.send(topicName, newConversation);
        return newConversation;
    }

 }

