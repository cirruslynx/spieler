// TODO: Remove these live tests

package com.cirruslynx.spieler.integration.test;

import com.cirruslynx.spieler.repository.*;
import com.cirruslynx.spieler.repository.config.PersistenceConfig;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@Log4j2
@Transactional
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = PersistenceConfig.class)
@SpringBootTest
@ComponentScan("com.cirruslynx.spieler.repository")
class SpielerDAOTest {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired

    private ConversationRepository conversationRepository;
    @Autowired
    private HeaderRepository headerRepository;
    @Autowired
    private ParagraphRepository paragraphRepository;
    @Autowired
    private TrailerRepository trailerRepository;

    public SpielerDAOTest()  { }

    @Test
    void givenConversationRepositoryWhenSavingConversationThenRetrieveConversation() {
        String conversationName = "TEST-CONVERSATION";
        Conversation conversation = new Conversation(conversationName);
        Header header = new Header(conversation,"HEADER1", LocalDateTime.now());
        Trailer trailer = new Trailer(conversation,"TRAILER 1",LocalDateTime.now());
        Paragraph paragraph = new Paragraph(conversation,"THIS IS PARAGRAPH1",LocalDateTime.now());
        List<Paragraph> paragraphs = new ArrayList<Paragraph>();
        paragraphs.add(paragraph);
        conversation.setHeader(header);
        conversation.setParagraphs(paragraphs);
        conversation.setTrailer(trailer);
        conversationRepository.save(conversation);

        Optional<Conversation> retrievedConversation;
        retrievedConversation = conversationRepository.findById(conversation.getId());
        assertEquals(conversation,retrievedConversation.get());
    }

    @Test
    void givenEmployeeRepositoryWhenSavingEmployeeThenRetrieveEmployee() {
        Employee employee = new Employee("Olivia", "Arico", "Queen of the North");
        employeeRepository.save(employee);
        Optional<Employee> retrievedEmployee;
        retrievedEmployee = employeeRepository.findById(employee.getId());
        assertEquals(employee,retrievedEmployee.get());
    }

    //@Test
    void givenParagraphRepositoryWhenSavingParagraphThenRetrieveParagraph() {
        String conversationName = "TEST-CONVERSATION";
        Conversation conversation = new Conversation(conversationName);
        Paragraph paragraph = new Paragraph(conversation,"THIS IS PARAGRAPH1",LocalDateTime.now());
        paragraphRepository.save(paragraph);
        Optional<Paragraph> retrievedParagraph;
        retrievedParagraph= paragraphRepository.findById(paragraph.getId());
        assertEquals(paragraph,retrievedParagraph.get());
    }
}