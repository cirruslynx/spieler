package com.cirruslynx.spieler.repository;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name="paragraph")
@ToString(exclude = {"conversation"})
@EqualsAndHashCode(exclude = {"conversation"})
@Getter @Setter
public class Paragraph {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private  long id;

	private  String text;

	private LocalDateTime createdDate;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "conversationId")
	private Conversation conversation;

	public Paragraph() {}

	public Paragraph(long id, Conversation conversation, String text, LocalDateTime createdDate) {
		this.id = id;
		this.conversation = conversation;
		this.text = text;
		this.createdDate = createdDate;
	}

	public Paragraph(Conversation conversation, String text, LocalDateTime createdDate) {
		this.conversation = conversation;
		this.text = text;
		this.createdDate = createdDate;
	}

}
