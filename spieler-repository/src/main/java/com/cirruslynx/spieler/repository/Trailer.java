package com.cirruslynx.spieler.repository;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;
@Entity(name="trailer")
@ToString(exclude = {"conversation"})
@EqualsAndHashCode(exclude = {"conversation"})
@Getter @Setter
public class Trailer {

	@Id
	@Column(name = "id", nullable = false)
	private  long id;
	private  String content;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "id")
	private Conversation conversation;

	private LocalDateTime createdDate;

	public Trailer() {}

	public Trailer (Long id, Conversation conversation, String content, LocalDateTime createdDate) {
		this.id = id;
		this.conversation = conversation;
		this.content = content;
		this.createdDate = createdDate;
	}

	public Trailer (Conversation conversation, String content, LocalDateTime createdDate) {
		this.conversation = conversation;
		this.content = content;
		this.createdDate = createdDate;
	}
}
