package com.cirruslynx.spieler.repository;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name="header")
@ToString(exclude = {"conversation"})
@EqualsAndHashCode()
@Getter @Setter
public class Header {

	@Id
	@Column(name = "id", nullable = false)
	private long id;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "id")
	private Conversation conversation;

	private String content;

	private LocalDateTime createdDate;

	public Header() {}

	public Header (Long id, Conversation conversation, String content, LocalDateTime createdDate) {
		this.id = id;
		this.conversation = conversation;
		this.content = content;
		this.createdDate = createdDate;
	}

	public Header(Conversation conversation, String content, LocalDateTime createdDate) {
		this.conversation = conversation;
		this.content = content;
		this.createdDate = createdDate;
	}

}
