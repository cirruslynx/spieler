package com.cirruslynx.spieler.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface HeaderRepository extends JpaRepository<Header, Long> {

   Optional<Header> findById(Long id);

}