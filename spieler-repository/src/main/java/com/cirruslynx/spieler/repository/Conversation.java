package com.cirruslynx.spieler.repository;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity(name="conversation")
@ToString
@EqualsAndHashCode
@Getter @Setter
public class Conversation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private  long id;

	private String name;

	@OneToOne(mappedBy="conversation", cascade = CascadeType.ALL, fetch= FetchType.LAZY, optional = false)
	private Header header;

	@OneToOne(mappedBy="conversation", cascade = CascadeType.ALL, fetch= FetchType.LAZY, optional = false)
	private Trailer trailer;

	@OneToMany(mappedBy = "conversation", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH,CascadeType.REFRESH })
	private List<Paragraph> paragraphs;

	public Conversation( String name) {
		this.name = name;
	}

	public Conversation(long id, String name, Header header, List<Paragraph> paragraphs, Trailer trailer) {
		this.id = id;
		this.name = name;
		this.header = header;
		this.paragraphs = paragraphs;
		this.trailer = trailer;
	}

	public Conversation(String name, Header header, List<Paragraph> paragraphs, Trailer trailer) {
		this.name = name;
		this.header = header;
		this.paragraphs = paragraphs;
		this.trailer = trailer;
	}

	public Conversation() {
		super();
	}
}
