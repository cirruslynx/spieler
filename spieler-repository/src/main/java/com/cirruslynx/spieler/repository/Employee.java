package com.cirruslynx.spieler.repository;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@ToString
@EqualsAndHashCode
@Entity(name="employee")
public class Employee {

	@Getter	@Setter private @Id @GeneratedValue(strategy= GenerationType.IDENTITY)  Long id;
	@Getter	@Setter private String firstName;
	@Getter	@Setter private String lastName;
	@Getter	@Setter private String description;
	@Getter	@Setter private @Version @JsonIgnore Long version;

	public Employee() {}

	public Employee(String firstName, String lastName, String description) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.description = description;
	}

}
