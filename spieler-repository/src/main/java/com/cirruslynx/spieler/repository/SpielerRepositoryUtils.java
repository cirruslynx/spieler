package com.cirruslynx.spieler.repository;

import com.cirruslynx.spieler.domain.ConversationDTO;
import com.cirruslynx.spieler.domain.HeaderDTO;
import com.cirruslynx.spieler.domain.ParagraphDTO;
import com.cirruslynx.spieler.domain.TrailerDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SpielerRepositoryUtils {

    private static Conversation toConversation(ConversationDTO conversationDTO) {
        Conversation con =  Optional.ofNullable(conversationDTO).isPresent() ?
                new Conversation(
                        conversationDTO.getId(),
                        conversationDTO.getName(),
                        toHeader(conversationDTO.getHeader()),
                        toParagraphs(conversationDTO.getParagraphs()),
                        toTrailer(conversationDTO.getTrailer())) : null;

        con.getHeader().setConversation(con);
        List<Paragraph> l = new ArrayList<>();
        for(Paragraph par : con.getParagraphs() ) {
            par.setConversation(con);
        }

        con.getTrailer().setConversation(con);
        return con;
    }

    public static ConversationDTO toConversationDTO(Conversation conversation){

        ConversationDTO con =  Optional.ofNullable(conversation).isPresent() ?
                new ConversationDTO(
                        conversation.getId(),
                        conversation.getName(),
                        toHeaderDTO(conversation.getHeader()),
                        toParagraphDTOs(conversation.getParagraphs()),
                        toTrailerDTO(conversation.getTrailer())) : null;

        con.getHeader().setConversation(con);
        List<Paragraph> l = new ArrayList<>();
        for(ParagraphDTO par : con.getParagraphs() ) {
            par.setConversation(con);
        }
        con.getTrailer().setConversation(con);
        return con;

    }

    public static Header toHeader(HeaderDTO headerDTO) {

        return Optional.ofNullable(headerDTO).isPresent() ?
                new Header(headerDTO.getId(), null, headerDTO.getContent(), headerDTO.getCreatedDate()) : null;

    }

    public static HeaderDTO toHeaderDTO(Header header){

        return Optional.ofNullable(header).isPresent() ?
                new HeaderDTO(header.getId(), null, header.getContent(), header.getCreatedDate()) : null;

    }


    public static Paragraph toParagraph(ParagraphDTO paragraphDTO) {

        return Optional.ofNullable(paragraphDTO).isPresent() ?
                new Paragraph(paragraphDTO.getId(), null, paragraphDTO.getText(), paragraphDTO.getCreatedDate()) : null;

    }

    public static ParagraphDTO toParagraphDTO(Paragraph paragraph){

        return Optional.ofNullable(paragraph).isPresent() ?
                new ParagraphDTO(paragraph.getId(), null, paragraph.getText(),paragraph.getCreatedDate()) : null;

    }

    private static List<Paragraph> toParagraphs(List<ParagraphDTO> paragraphDTOs) {
        List<Paragraph> l = new ArrayList<>();
            for(ParagraphDTO par : paragraphDTOs ) {
                Paragraph p = toParagraph(par);
                l.add(p);
            }
            return l;
    }

    private static List<ParagraphDTO> toParagraphDTOs(List<Paragraph> paragraphs) {
        List<ParagraphDTO> l = new ArrayList<>();
        for(Paragraph par : paragraphs ) {
            ParagraphDTO p = toParagraphDTO(par);
            l.add(p);
        };
        return l;
    }

    public static Trailer toTrailer(TrailerDTO trailerDTO) {

        return Optional.ofNullable(trailerDTO).isPresent() ?
                new Trailer(trailerDTO.getId(),null,trailerDTO.getContent(), trailerDTO.getCreatedDate()) : null;
    }

    public static TrailerDTO toTrailerDTO(Trailer trailer){

        return Optional.ofNullable(trailer).isPresent() ?
                new TrailerDTO(trailer.getId(), null, trailer.getContent(), trailer.getCreatedDate()) : null;

    }

}
