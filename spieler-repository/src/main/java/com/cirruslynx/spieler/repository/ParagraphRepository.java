package com.cirruslynx.spieler.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ParagraphRepository extends JpaRepository<Paragraph, Long> {

   Optional<Paragraph> findById(Long id);

}