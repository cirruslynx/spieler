package com.cirruslynx.spieler.repository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpielerRepositoryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpielerRepositoryServiceApplication.class, args);
	}
}
