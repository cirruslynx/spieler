package com.cirruslynx.spieler.repository.controller;

import com.cirruslynx.spieler.repository.Conversation;
import com.cirruslynx.spieler.repository.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleAfterDelete;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.hateoas.server.EntityLinks;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import static com.cirruslynx.spieler.repository.config.WebSocketConfiguration.MESSAGE_PREFIX;

@Component
@org.springframework.data.rest.core.annotation.RepositoryEventHandler()
public class RepositoryEventHandler {

	private final SimpMessagingTemplate websocket;

	private final EntityLinks entityLinks;

	@Autowired
	public RepositoryEventHandler(SimpMessagingTemplate websocket, EntityLinks entityLinks) {
		this.websocket = websocket;
		this.entityLinks = entityLinks;
	}

	@HandleAfterCreate
	public void newEmployee(Employee employee) {
		this.websocket.convertAndSend(
				MESSAGE_PREFIX + "/newEmployee", getPath(employee));
	}

	@HandleAfterDelete
	public void deleteEmployee(Employee employee) {
		this.websocket.convertAndSend(
				MESSAGE_PREFIX + "/deleteEmployee", getPath(employee));
	}

	@HandleAfterSave
	public void updateEmployee(Employee employee) {
		this.websocket.convertAndSend(
				MESSAGE_PREFIX + "/updateEmployee", getPath(employee));
	}

	/**
	 * Take an {@link Employee} and get the URI using Spring Data REST's {@link EntityLinks}.
	 *
	 * @param employee
	 */
	private String getPath(Employee employee) {
		return this.entityLinks.linkForItemResource(employee.getClass(),
				employee.getId()).toUri().getPath();
	}
	
	
	@HandleAfterCreate
	public void newConversation(Conversation conversation) {
		this.websocket.convertAndSend(
				MESSAGE_PREFIX + "/newConversation", getPath(conversation));
	}

	@HandleAfterDelete
	public void deleteConversation(Conversation conversation) {
		this.websocket.convertAndSend(
				MESSAGE_PREFIX + "/deleteConversation", getPath(conversation));
	}

	@HandleAfterSave
	public void updateConversation(Conversation conversation) {
		this.websocket.convertAndSend(
				MESSAGE_PREFIX + "/updateConversation", getPath(conversation));
	}

	/**
	 * Take an {@link Conversation} and get the URI using Spring Data REST's {@link EntityLinks}.
	 *
	 * @param conversation
	 */
	private String getPath(Conversation conversation) {
		return this.entityLinks.linkForItemResource(conversation.getClass(),
				conversation.getId()).toUri().getPath();
	}

}