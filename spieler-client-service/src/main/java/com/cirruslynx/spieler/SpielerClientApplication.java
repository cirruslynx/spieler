package com.cirruslynx.spieler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpielerClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpielerClientApplication.class, args);
    }

}
