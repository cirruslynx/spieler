package com.cirruslynx.spieler.controller;

import com.cirruslynx.spieler.domain.ConversationDTO;
import com.cirruslynx.spieler.service.IConversationClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;

@RestController
public class SpielerClientController {

	private final IConversationClientService spielerClientService;

	@Autowired
	public SpielerClientController(IConversationClientService spielerClientService) {
		this.spielerClientService = spielerClientService;
	}

	@RolesAllowed("user")
	@PostMapping("/conversation")
	public ConversationDTO createConversation(@RequestParam(value = "name", defaultValue = "CONVERSATION") String name) {
		return spielerClientService.createConversation(name);
	}
}
