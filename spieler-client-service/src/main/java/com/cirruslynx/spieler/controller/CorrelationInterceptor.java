package com.cirruslynx.spieler.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.log4j.Log4j2;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.AsyncHandlerInterceptor;


import com.cirruslynx.utilities.RandomIdGenerator;
import static com.cirruslynx.utilities.CorrelationIdConstants.CORRELATION_ID_HEADER_NAME;
import static com.cirruslynx.utilities.CorrelationIdConstants.CORRELATION_ID_LOG_VAR_NAME;

@Log4j2
@Component
@ComponentScan("com.cirruslynx.utilities")
public class CorrelationInterceptor  implements AsyncHandlerInterceptor {

    private final RandomIdGenerator RandomIdGenerator;

    @Autowired
    public CorrelationInterceptor(RandomIdGenerator RandomIdGenerator) {
        this.RandomIdGenerator = RandomIdGenerator;
    }

    @Override
    public boolean preHandle(final HttpServletRequest request, 
    						 final HttpServletResponse response,
                             final Object handler) {
        final String correlationId = getCorrelationIdFromHeader(request);
        MDC.put(CORRELATION_ID_LOG_VAR_NAME, correlationId);
        response.setHeader(CORRELATION_ID_HEADER_NAME,correlationId);
        log.debug("Using correlationId {}", correlationId);
        return true;
    }

    @Override
    public void afterCompletion(final HttpServletRequest request,
    							final HttpServletResponse response,
                                final Object handler,
                                final Exception ex) {
    }

    private String getCorrelationIdFromHeader(final HttpServletRequest request) {
        String correlationId = request.getHeader(CORRELATION_ID_HEADER_NAME);
        correlationId = RandomIdGenerator.verifyOrCreateId(correlationId);
        return correlationId;
    }

}