package com.cirruslynx.spieler.service;

import com.cirruslynx.spieler.domain.ConversationDTO;

public interface IConversationClientService {
    ConversationDTO createConversation(String name);
}
