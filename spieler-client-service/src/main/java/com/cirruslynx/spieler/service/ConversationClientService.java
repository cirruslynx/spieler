package com.cirruslynx.spieler.service;

import com.cirruslynx.spieler.domain.ConversationDTO;
import com.cirruslynx.spieler.domain.HeaderDTO;
import com.cirruslynx.spieler.security.KeyCloakTokenManager;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Log4j2
@Profile("prod")
@Service
public class ConversationClientService implements IConversationClientService {

	@Value("${spieler.core.port}")
	private String spielerPort;
	
	@Value("${spieler.core.server}")
	private String spielerServer;
	
	@Override
	public ConversationDTO createConversation(final String name) {

		log.debug("ConversationClientService is creating conversation {}", name);
		KeyCloakTokenManager tokenManager = new KeyCloakTokenManager();
		String accessToken =  tokenManager.getAccessToken();
		String coreURL = "http://" +  spielerServer + ":" +  spielerPort;

		Mono<ConversationDTO> conversation =
				WebClient.create(coreURL)
						.post()
						.uri(uriBuilder -> uriBuilder.path("/conversation")
								.queryParam("name", name)
								.build())
						.header("Authorization", "bearer " + accessToken)
						.retrieve()
						.bodyToMono(ConversationDTO.class);
		return conversation.block();
	}

}
