package com.cirruslynx.spieler.service;

import com.cirruslynx.spieler.domain.ConversationDTO;
import com.cirruslynx.spieler.domain.HeaderDTO;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Log4j2
@Profile("dev")
@Service
public class AnonymousConversationClientService implements IConversationClientService {

	@Value("${spieler.core" +
			".server.port}")
	private String spielerPort;
	
	@Value("${spieler.core.server}")
	private String spielerServer;

	@Override
	public ConversationDTO createConversation(final String name) {

		log.debug("SpielerClientService created conversation {}", name);
		String backEndHeaderURL = "http://" +  spielerServer + ":" +  spielerPort;
		Mono<ConversationDTO> conversation =
				WebClient.create(backEndHeaderURL)
						.post()
						.uri(uriBuilder -> uriBuilder.path("/conversation")
								.queryParam("name", name)
								.build())
						.retrieve()
						.bodyToMono(ConversationDTO.class);
		return conversation.block();
	}
}
