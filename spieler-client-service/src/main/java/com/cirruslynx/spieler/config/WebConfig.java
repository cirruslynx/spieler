package com.cirruslynx.spieler.config;

import com.cirruslynx.spieler.controller.CorrelationInterceptor;
import com.cirruslynx.utilities.RandomIdGenerator;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    private RandomIdGenerator RandomIdGenerator;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new CorrelationInterceptor(new RandomIdGenerator()));
    }

}