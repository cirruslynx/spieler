package com.cirruslynx.spieler.controller;

import com.cirruslynx.spieler.SpielerClientApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = SpielerClientApplication.class)
class SpielerClientControllerSmokeTest {

	@Autowired
	private SpielerClientController controller;

	@Test
	void contextLoads() {
		assertThat(controller).isNotNull();
	}
}