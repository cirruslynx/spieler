package com.cirruslynx.spieler.service;

import com.cirruslynx.spieler.domain.ConversationDTO;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Log4j2
@Service
public class SpielerConsumerService {

	@Autowired
	ConsumerFactory<String, ConversationDTO> kafkaConsumerFactory;

	@Value("${spieler.kafka.topic.name}")
	private String topicName;

	@PostConstruct
	private void postConstruct() {


		ContainerProperties containerProperties = new ContainerProperties(topicName);
		containerProperties.setMessageListener(
				(MessageListener<String, ConversationDTO>) consumerRecord -> conversationListener(consumerRecord.value()));
		ConcurrentMessageListenerContainer container =
				new ConcurrentMessageListenerContainer<>(
						kafkaConsumerFactory,
						containerProperties);
		container.start();
	}

	public void	conversationListener(ConversationDTO conversationDTO)
	{
		log.debug("Get message id {}  with content {} from topic {}", conversationDTO.getId(), conversationDTO.getName(), topicName);
	}
}