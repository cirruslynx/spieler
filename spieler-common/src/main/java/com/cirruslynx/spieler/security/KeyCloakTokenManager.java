package com.cirruslynx.spieler.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.net.URI;

@Component
public class KeyCloakTokenManager
{
    public String getAccessToken() {
        final String keycloakTokenUri = "http://localhost:8787/realms/Demo-Realm/protocol/openid-connect/token";
        final String password= "password";

        // get access token
        MultiValueMap<String, String> bodyValues = new LinkedMultiValueMap<>();
        bodyValues.add("grant_type", password);
        bodyValues.add("client_id", "springboot-microservice");
        bodyValues.add("client_secret", "fb2K3eS4EQTdLTeIhaZTaCInOC6eHcXW");
        bodyValues.add("username", "employee1");
        bodyValues.add("password", password);

        WebClient client = WebClient.create();

        String accessToken = null;
        try {
            String accessTokenJSON = client.post()
                    .uri(new URI(keycloakTokenUri))
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .accept(MediaType.APPLICATION_JSON)
                    .body(BodyInserters.fromFormData(bodyValues))
                    .retrieve()
                    .bodyToMono(String.class)
                    .block();

            final ObjectNode node = new ObjectMapper().readValue(accessTokenJSON, ObjectNode.class);
            if (node.has("access_token")) {
                accessToken = node.get("access_token").textValue();
            }
            else {
                throw new RuntimeException("No access token retrieved");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return accessToken;
    }
}
