package com.cirruslynx.spieler.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@ToString(exclude = {"conversation"})
@EqualsAndHashCode(exclude = {"conversation"})
@Getter
@Setter
public class TrailerDTO {

	private long id;

	@JsonIgnore
	private ConversationDTO conversation;

	private String content;

	private LocalDateTime createdDate;

	public TrailerDTO(long id, ConversationDTO conversation, String content, LocalDateTime createdDate) {
		this.id = id;
		this.content = content;
		this.createdDate = createdDate;
	}

	public TrailerDTO() {
		super();
	}
}
