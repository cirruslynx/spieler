package com.cirruslynx.spieler.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@ToString(exclude = {"conversation"})
@EqualsAndHashCode(exclude = {"conversation"})
@Getter @Setter
public class ParagraphDTO {

	private  long id;

	@JsonIgnore
	private ConversationDTO conversation;

	private  String text;

	private LocalDateTime createdDate;

	public ParagraphDTO(long id, ConversationDTO conversation,String text, LocalDateTime createdDate) {
		this.id = id;
		this.conversation = conversation;
		this.text = text;
		this.createdDate = createdDate;
	}

	public ParagraphDTO() {}

}
