package com.cirruslynx.spieler.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@EqualsAndHashCode
@Getter	@Setter
public class ConversationDTO {

	private  long id;

	private String name;

	private HeaderDTO header;

	private List<ParagraphDTO> paragraphs;

	private TrailerDTO trailer;

	public ConversationDTO(long id, String name, HeaderDTO header, List<ParagraphDTO> paragraphs, TrailerDTO trailer) {
		this.id = id;
		this.name = name;
		this.header = header;
		this.paragraphs = paragraphs;
		this.trailer = trailer;
	}

	public ConversationDTO() {
		super();
	}
}
