package com.cirruslynx.spieler.web.controller;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.log4j.Log4j2;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.annotation.Order;

import com.cirruslynx.utilities.RandomIdGenerator;
import org.springframework.stereotype.Component;

import static com.cirruslynx.utilities.CorrelationIdConstants.CORRELATION_ID_HEADER_NAME;
import static com.cirruslynx.utilities.CorrelationIdConstants.CORRELATION_ID_LOG_VAR_NAME;


@Log4j2
@Order(100)
@Component
@ComponentScan("com.cirruslynx.utilities")
public class CorrelationIdFilter<MutableHttpServletRequest> implements Filter
{

   @Autowired
   public CorrelationIdFilter(RandomIdGenerator RandomIdGenerator) {
      this.RandomIdGenerator = RandomIdGenerator;
   }

   private final RandomIdGenerator RandomIdGenerator;
	
   @Override
   public void init(FilterConfig filterConfig) { /* No implementation needed */ }

   @Override
   public void doFilter(ServletRequest request, ServletResponse response,FilterChain filterChain)  throws IOException, ServletException
   {
      String correlationId = ((HttpServletRequest) request).getHeader(CORRELATION_ID_HEADER_NAME);
      correlationId = RandomIdGenerator.verifyOrCreateId(correlationId);
      log.debug("Using correlation id {} ", correlationId);
      MDC.put(CORRELATION_ID_LOG_VAR_NAME, correlationId);
      HttpServletResponse httpServletResponse=(HttpServletResponse)response;
      log.trace("Setting  header correlationid to {} ", correlationId);
      httpServletResponse.setHeader(CORRELATION_ID_HEADER_NAME,correlationId);
      filterChain.doFilter(request, response);
   }

   @Override public void destroy()  { /* No implementation needed */ }

}