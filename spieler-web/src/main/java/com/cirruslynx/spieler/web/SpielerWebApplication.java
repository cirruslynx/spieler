package com.cirruslynx.spieler.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpielerWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpielerWebApplication.class, args);
	}
}
