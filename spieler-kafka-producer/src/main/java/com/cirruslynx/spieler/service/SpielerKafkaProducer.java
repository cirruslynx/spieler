package com.cirruslynx.spieler.service;

import com.cirruslynx.spieler.domain.ConversationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class SpielerKafkaProducer {

    private final KafkaTemplate<String, ConversationDTO> template;

    @Autowired
    public SpielerKafkaProducer(KafkaTemplate<String, ConversationDTO> template) {
        this.template = template;
    }

    public void send(String topicName, ConversationDTO data) {
        template.send(topicName, data);
    }
}
